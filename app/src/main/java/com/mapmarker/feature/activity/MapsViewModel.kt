package com.mapmarker.feature.activity

import com.mapmarker.common.data.preferance.AppRepository
import com.mapmarker.common.helper.SchedulerHelper
import com.mapmarker.feature.base.BaseViewModel

class MapsViewModel(private val appRepository: AppRepository,
                    private val scheduler: SchedulerHelper)

    : BaseViewModel<MapsNavigator>(appRepository, scheduler) {

    fun getWeatherList() {
        getNavigator().showLoading()
        mCompositeDisposable.add(mRepository.getWeatherList()
            .subscribeOn(mRxSchedule.io())
            .observeOn(mRxSchedule.ui())
            .subscribe({ response ->
                if (response.cod == 200) {
                    getNavigator().hideLoading()
                    print("Success")
                    getNavigator().sendResponseBack(response)
                } else {
                    getNavigator().hideLoading()
                    print("Success Error")
                }

            }, { throwable ->
                print("Success" + throwable)
                getNavigator().hideLoading()
                getNavigator().handleError(throwable)
            }))
    }

}