package com.mapmarker.feature.activity

import com.mapmarker.common.data.model.response.ListResponseModel

interface MapsNavigator {

    fun handleError(throwable: Throwable)

    fun showMessage(message: String)

    fun showLoading()

    fun hideLoading()

    fun sendResponseBack(response: ListResponseModel)
}