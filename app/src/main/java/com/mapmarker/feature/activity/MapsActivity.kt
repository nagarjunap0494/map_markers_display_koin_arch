package com.mapmarker.feature.activity

import android.os.Bundle

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.mapmarker.BR
import com.mapmarker.R
import com.mapmarker.common.data.model.response.ListResponseModel
import com.mapmarker.databinding.ActivityMapsBinding
import com.mapmarker.feature.base.BaseActivity
import org.koin.androidx.viewmodel.ext.android.viewModel

class MapsActivity : BaseActivity<ActivityMapsBinding, MapsViewModel>(), MapsNavigator, OnMapReadyCallback {

    private val mapsViewModel: MapsViewModel by viewModel()
    private lateinit var splashBinding: ActivityMapsBinding

    override fun getBindingVariable(): Int = BR.viewModel
    override fun getLayoutId(): Int = R.layout.activity_maps
    override fun getMyViewModel(): MapsViewModel = mapsViewModel

    private var mGoogleMap: GoogleMap? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        splashBinding = getViewDataBinding()
        splashBinding.setLifecycleOwner(this)
        mapsViewModel.setNavigator(this)
        setUp()
    }
    override fun setUp() {
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment!!.getMapAsync(this)

        mapsViewModel.getWeatherList()

    }

    override fun onMapReady(googleMap: GoogleMap) {
        mGoogleMap = googleMap

        val sydney = LatLng(-34.0, 151.0)
        mGoogleMap!!.addMarker(MarkerOptions().position(sydney).title("Marker"))
        mGoogleMap!!.moveCamera(CameraUpdateFactory.newLatLng(sydney))
    }

    override fun sendResponseBack(responseModel: ListResponseModel) {
        print(" ======> " + responseModel.list.size)

        for (i in 0 until responseModel.list.size) {
            println(i)

            val sydney = LatLng(responseModel.list[i].coord.Lat!!, responseModel.list[i].coord.Lon!!)
            mGoogleMap!!.addMarker(MarkerOptions().position(sydney).title(responseModel.list[i].name))
            mGoogleMap!!.moveCamera(CameraUpdateFactory.newLatLng(sydney))
        }
    }
}