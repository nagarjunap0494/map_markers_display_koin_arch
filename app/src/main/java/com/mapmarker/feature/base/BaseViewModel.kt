package com.mapmarker.feature.base

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.ViewModel
import com.mapmarker.common.data.preferance.AppRepository
import com.mapmarker.common.helper.SchedulerHelper
import io.reactivex.disposables.CompositeDisposable
import java.lang.ref.WeakReference

abstract class BaseViewModel<N>(appRepository: AppRepository, rxSchedule: SchedulerHelper) : ViewModel() {

    private lateinit var mNavigator: WeakReference<N>
    val mRepository = appRepository
    val mRxSchedule = rxSchedule
    val mCompositeDisposable = CompositeDisposable()
    var mIsLoading = ObservableBoolean(false)

    override fun onCleared() {
        mCompositeDisposable.dispose()
        super.onCleared()
    }

    fun getIsLoading(): ObservableBoolean {
        return mIsLoading
    }

    fun setIsLoading(isLoading: Boolean) {
        mIsLoading.set(isLoading)
    }

    fun getNavigator(): N = mNavigator.get()!!


    fun setNavigator(navigator: N) {
        this.mNavigator = WeakReference(navigator)
    }
}