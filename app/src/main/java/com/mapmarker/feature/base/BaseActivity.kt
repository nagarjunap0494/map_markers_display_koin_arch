package com.mapmarker.feature.base

import android.annotation.TargetApi
import android.app.ProgressDialog
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.google.android.material.snackbar.Snackbar
import com.mapmarker.R
import com.mapmarker.feature.utils.AppUtils
import retrofit2.HttpException
import java.io.IOException
import java.net.SocketTimeoutException

abstract class BaseActivity<T : ViewDataBinding, V : BaseViewModel<*>> : AppCompatActivity(), BaseFragment.Callback {
    private var mProgressDialog: ProgressDialog? = null
    private lateinit var mViewDataBinding: T
    private var mViewModel: V? = null
    fun getViewDataBinding(): T = mViewDataBinding

    override fun onFragmentAttached() {}
    override fun onFragmentDetached(tag: String) {}
    abstract fun getBindingVariable(): Int
    abstract fun getLayoutId(): Int
    abstract fun getMyViewModel(): V

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        performDataBinding()
    }


    private fun performDataBinding() {
        mViewDataBinding = DataBindingUtil.setContentView(this, getLayoutId())
        this.mViewModel = if (mViewModel == null) getMyViewModel() else mViewModel
        mViewDataBinding.setVariable(getBindingVariable(), mViewModel)
        mViewDataBinding.executePendingBindings()
    }

    @TargetApi(Build.VERSION_CODES.M)
    fun hasPermission(permission: String): Boolean {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M || checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED
    }

    @TargetApi(Build.VERSION_CODES.M)
    fun requestPermissionsSafely(permissions: Array<String>, requestCode: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissions, requestCode)
        }
    }

    fun hideKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
            imm?.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun hideLoading() {
        if (mProgressDialog != null && mProgressDialog?.isShowing!!) {
            mProgressDialog?.cancel()
        }
    }

    fun showLoading() {
        hideLoading()
        mProgressDialog = AppUtils.showLoadingDialog(this)
    }

    fun isNetworkConnected(): Boolean {
        return AppUtils.isNetworkConnected(applicationContext)
    }

    fun showMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    fun onError(message: String?) {
        if (message != null) {
            showSnackBar(message)
        } else {
            showSnackBar(getString(R.string.some_error))
        }
    }

    fun showSnackBar(message: String) {
        val snackbar = Snackbar.make(findViewById(android.R.id.content),
                message, Snackbar.LENGTH_SHORT)
        val sbView = snackbar.view
        sbView.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
        val textView = sbView
                .findViewById<View>(R.id.snackbar_text) as TextView
        textView.setTextColor(ContextCompat.getColor(this, R.color.colorAccent))
        textView.textSize = 16F
        snackbar.show()
    }

    abstract fun setUp()

    fun handleError(throwable: Throwable) {
        when (throwable) {
            is HttpException -> handleHttpError(throwable)
            is IOException -> handleIOError()
            is SocketTimeoutException -> showMessage(getString(R.string.check_internet_connection))
            else -> showMessage(getString(R.string.some_error))
        }
    }

    private fun handleIOError() {
        showMessage(getString(R.string.check_internet_connection))
    }

    private fun handleHttpError(throwable: HttpException) {
        // We had non-200 http error here
        //error body
        val errorBody = throwable.response().errorBody().string()
        var message: String = AppUtils.parseErrorBody(errorBody)?.message
                ?: getString(R.string.some_error)

        when (throwable.code()) {
            400 -> showMessage(message)
            401 -> showMessage(message)
            409 -> showMessage(message)
            500 -> showMessage(getString(R.string.error_500))
            404 -> showMessage(message)
            else -> showMessage(getString(R.string.some_error))
        }
    }
}