package com.mapmarker.feature.utils

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.text.TextUtils
import android.util.Patterns
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import com.google.gson.GsonBuilder
import com.google.gson.JsonSyntaxException
import java.lang.Exception
import com.mapmarker.R
import com.mapmarker.common.data.model.others.ApiErrorBody

/**
 * Created by Nagarjuna on 24/06/19.
 */
class AppUtils {

    companion object {

        fun showLoadingDialog(context: Context): ProgressDialog {
            val progressDialog = ProgressDialog(context)
            progressDialog.show()
            if (progressDialog.window != null) {
                progressDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            }
            progressDialog.setContentView(R.layout.progress_dialog)
            progressDialog.isIndeterminate = true
            progressDialog.setCancelable(false)
            progressDialog.setCanceledOnTouchOutside(false)
            return progressDialog
        }

        fun isNetworkConnected(context: Context): Boolean {
            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
            if (cm != null) {
                val activeNetwork = cm.activeNetworkInfo
                return activeNetwork != null && activeNetwork.isConnectedOrConnecting
            }
            return false
        }

        fun isNullOrEmpty(input: String?): Boolean {
            return input == null || TextUtils.isEmpty(input)
        }

        fun isEmailValid(email: String?): Boolean {
            return Patterns.EMAIL_ADDRESS.matcher(email).matches()
        }

        fun isPhoneNumberValid(phone: String?): Boolean {
            return Patterns.PHONE.matcher(phone).matches()
        }

        fun textField(txtField: EditText): String? {
            return txtField.text.toString();
        }

        fun parseErrorBody(errorBody: String): ApiErrorBody? {
            var apiError: ApiErrorBody? = null
            val gson = GsonBuilder().create()
            try {
                apiError = gson.fromJson(errorBody, ApiErrorBody::class.java)
            } catch (e: JsonSyntaxException) {
                e.printStackTrace()
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return apiError
        }

        fun hideSoftKeyboard(activity: Activity) {
            val inputMethodManager: InputMethodManager =
                activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(activity.currentFocus!!.windowToken, 0)
        }
    }
}