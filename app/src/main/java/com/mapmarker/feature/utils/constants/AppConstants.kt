package com.mapmarker.feature.utils.constants

class AppConstants {

    companion object {

        const val PREF_NAME = "MapMarker_preference"

        const val NULL_INDEX = -1

        /*headers*/
        //Body
        const val HEADER_BODY_ACCEPT = "application/json"
        const val HEADER_BODY_CONTENT_TYPE = "application/json"

        //Keys
        const val HEADER_KEY_ACCEPT = "Accept"
        const val HEADER_KEY_CONTENT_TYPE = "Content-Type"
        const val HEADER_KEY_AUTHORIZATION = "Authorization"

        /*Network request time out*/
        const val NETWORK_REQUEST_TIMEOUT = 60L

    }

}