package com.mapmarker.feature.utils.constants

class PreferenceConstants {

    companion object {

        /*constants for shared preferences Keys*/
        val PREF_KEY_ACCESS_TOKEN = "PREF_KEY_ACCESS_TOKEN"
        val PREF_KEY_REFRESH_ACCESS_TOKEN = "PREF_KEY_REFRESH_ACCESS_TOKEN"

    }
}