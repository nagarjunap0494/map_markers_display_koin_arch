package com.mapmarker.common.helper

import io.reactivex.Scheduler

interface SchedulerHelper {
    fun io(): Scheduler
    fun ui(): Scheduler
    fun computation(): Scheduler
}
