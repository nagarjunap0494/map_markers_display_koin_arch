package com.mapmarker.common.data.preferance

import com.mapmarker.common.data.api.ApiHelper


interface AppRepositoryHelper : PreferencesHelper, ApiHelper {

}