package com.mapmarker.common.data.model.others

data class ApiErrorBody(var success: Boolean, var status:Int, var message: String) {
}