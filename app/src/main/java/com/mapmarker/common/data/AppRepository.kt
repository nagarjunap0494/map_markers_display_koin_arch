package com.mapmarker.common.data.preferance

import android.content.Context
import com.mapmarker.common.data.api.ApiHelper
import com.mapmarker.common.data.model.response.ListResponseModel
import io.reactivex.Single

class AppRepository constructor(
    private val mContext: Context,
    private val mAppPreference: AppPreference,
    private val mAppApiService: ApiHelper
) : AppRepositoryHelper {

    override fun getWeatherList(): Single<ListResponseModel> {
        return mAppApiService.getWeatherList()
    }
}