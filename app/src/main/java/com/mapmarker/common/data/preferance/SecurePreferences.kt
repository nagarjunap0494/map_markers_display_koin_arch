package com.mapmarker.common.data.preferance

import android.annotation.TargetApi
import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import android.provider.Settings
import android.util.Base64
import android.util.Log
import com.mapmarker.feature.utils.constants.AppConstants

import java.security.NoSuchAlgorithmException
import java.security.SecureRandom
import java.security.spec.InvalidKeySpecException
import java.util.HashMap
import java.util.HashSet

import javax.crypto.Cipher
import javax.crypto.KeyGenerator
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.PBEKeySpec
import javax.crypto.spec.SecretKeySpec

class SecurePreferences(context: Context) : SharedPreferences {

    init {
        // Proxy design pattern
        if (SecurePreferences.sFile == null) {
            SecurePreferences.sFile = context.getSharedPreferences(AppConstants.PREF_NAME,Context.MODE_PRIVATE)
        }
        // Initialize encryption/decryption key
        try {
            val key = SecurePreferences.generateAesKeyName(context)
            var value = SecurePreferences.sFile!!.getString(key, null)
            if (value == null) {
                value = SecurePreferences.generateAesKeyValue()
                SecurePreferences.sFile!!.edit().putString(key, value).commit()
            }
            SecurePreferences.sKey = SecurePreferences.decode(value)
        } catch (e: Exception) {
            throw IllegalStateException(e)
        }

    }

    override fun getAll(): Map<String, String> {
        val encryptedMap = SecurePreferences.sFile!!.all
        val decryptedMap = HashMap<String, String>(encryptedMap.size)
        for ((key, value) in encryptedMap) {
            try {
                decryptedMap[SecurePreferences.decrypt(key)!!] = SecurePreferences.decrypt(value.toString())!!
            } catch (e: Exception) {
                // Ignore unencrypted key/value pairs
            }

        }
        return decryptedMap
    }

    override fun getString(key: String, defaultValue: String?): String? {
        val encryptedValue = SecurePreferences.sFile!!.getString(SecurePreferences.encrypt(key), null)
        return if (encryptedValue != null) SecurePreferences.decrypt(encryptedValue) else defaultValue
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    override fun getStringSet(key: String, defaultValues: Set<String>?): Set<String>? {
        val encryptedSet = SecurePreferences.sFile!!.getStringSet(SecurePreferences.encrypt(key), null)
                ?: return defaultValues
        val decryptedSet = HashSet<String>(encryptedSet.size)
        for (encryptedValue in encryptedSet) {
            decryptedSet.add(SecurePreferences.decrypt(encryptedValue)!!)
        }
        return decryptedSet
    }

    override fun getInt(key: String, defaultValue: Int): Int {
        val encryptedValue = SecurePreferences.sFile!!.getString(SecurePreferences.encrypt(key), null)
                ?: return defaultValue
        try {
            return Integer.parseInt(SecurePreferences.decrypt(encryptedValue)!!)
        } catch (e: NumberFormatException) {
            throw ClassCastException(e.message)
        }

    }

    override fun getLong(key: String, defaultValue: Long): Long {
        val encryptedValue = SecurePreferences.sFile!!.getString(SecurePreferences.encrypt(key), null)
                ?: return defaultValue
        try {
            return java.lang.Long.parseLong(SecurePreferences.decrypt(encryptedValue)!!)
        } catch (e: NumberFormatException) {
            throw ClassCastException(e.message)
        }

    }

    override fun getFloat(key: String, defaultValue: Float): Float {
        val encryptedValue = SecurePreferences.sFile!!.getString(SecurePreferences.encrypt(key), null)
                ?: return defaultValue
        try {
            return java.lang.Float.parseFloat(SecurePreferences.decrypt(encryptedValue)!!)
        } catch (e: NumberFormatException) {
            throw ClassCastException(e.message)
        }

    }

    override fun getBoolean(key: String, defaultValue: Boolean): Boolean {
        val encryptedValue = SecurePreferences.sFile!!.getString(SecurePreferences.encrypt(key), null)
                ?: return defaultValue
        try {
            return java.lang.Boolean.parseBoolean(SecurePreferences.decrypt(encryptedValue))
        } catch (e: NumberFormatException) {
            throw ClassCastException(e.message)
        }

    }

    override fun contains(key: String): Boolean {
        return SecurePreferences.sFile!!.contains(SecurePreferences.encrypt(key))
    }

    override fun edit(): Editor {
        return Editor()
    }

    /**
     * Wrapper for Android's [SharedPreferences.Editor].
     *
     *
     * Used for modifying values in a [SecurePreferences] object. All changes you make in an
     * editor are batched, and not copied back to the original [SecurePreferences] until you
     * call [.commit] or [.apply].
     */
    class Editor
    /**
     * Constructor.
     */
    internal constructor() : SharedPreferences.Editor {
        private val mEditor: SharedPreferences.Editor

        init {
            mEditor = SecurePreferences.sFile!!.edit()
        }

        override fun putString(key: String, value: String?): SharedPreferences.Editor {
            mEditor.putString(SecurePreferences.encrypt(key), SecurePreferences.encrypt(value))
            Log.d(SecurePreferences.encrypt(key), SecurePreferences.encrypt(value))
            return this
        }

        @TargetApi(Build.VERSION_CODES.HONEYCOMB)
        override fun putStringSet(key: String, values: Set<String>?): SharedPreferences.Editor {
            val encryptedValues = HashSet<String>(values!!.size)
            for (value in values) {
                encryptedValues.add(SecurePreferences.encrypt(value)!!)
            }
            mEditor.putStringSet(SecurePreferences.encrypt(key), encryptedValues)
            return this
        }

        override fun putInt(key: String, value: Int): SharedPreferences.Editor {
            mEditor.putString(SecurePreferences.encrypt(key),
                    SecurePreferences.encrypt(Integer.toString(value)))
            return this
        }

        override fun putLong(key: String, value: Long): SharedPreferences.Editor {
            mEditor.putString(SecurePreferences.encrypt(key),
                    SecurePreferences.encrypt(java.lang.Long.toString(value)))
            return this
        }

        override fun putFloat(key: String, value: Float): SharedPreferences.Editor {
            mEditor.putString(SecurePreferences.encrypt(key),
                    SecurePreferences.encrypt(java.lang.Float.toString(value)))
            return this
        }

        override fun putBoolean(key: String, value: Boolean): SharedPreferences.Editor {
            mEditor.putString(SecurePreferences.encrypt(key),
                    SecurePreferences.encrypt(java.lang.Boolean.toString(value)))
            return this
        }

        override fun remove(key: String): SharedPreferences.Editor {
            mEditor.remove(SecurePreferences.encrypt(key))
            return this
        }

        override fun clear(): SharedPreferences.Editor {
            mEditor.clear()
            return this
        }

        override fun commit(): Boolean {
            return mEditor.commit()
        }

        @TargetApi(Build.VERSION_CODES.GINGERBREAD)
        override fun apply() {
            mEditor.apply()
        }
    }

    override fun registerOnSharedPreferenceChangeListener(listener: SharedPreferences.OnSharedPreferenceChangeListener) {
        SecurePreferences.sFile!!.registerOnSharedPreferenceChangeListener(listener)
    }

    override fun unregisterOnSharedPreferenceChangeListener(listener: SharedPreferences.OnSharedPreferenceChangeListener) {
        SecurePreferences.sFile!!.unregisterOnSharedPreferenceChangeListener(listener)
    }

    companion object {

        const val ENCRYPTION_TYPE: String = "AES"
        const val SECRETE_KEY: String = "PBKDF2WithHmacSHA1"
        const val ENCRYPT_MODE: String = "encrypt"
        const val DECRYPT_MODE: String = "decrypt"

        private var sFile: SharedPreferences? = null
        private var sKey: ByteArray? = null

        private fun encode(input: ByteArray): String {
            return Base64.encodeToString(input, Base64.NO_PADDING or Base64.NO_WRAP)
        }

        private fun decode(input: String): ByteArray {
            return Base64.decode(input, Base64.NO_PADDING or Base64.NO_WRAP)
        }

        @Throws(InvalidKeySpecException::class, NoSuchAlgorithmException::class)
        private fun generateAesKeyName(context: Context): String {
            val password = context.packageName.toCharArray()
            val salt = Settings.Secure.getString(context.contentResolver,
                    Settings.Secure.ANDROID_ID).toByteArray()

            // Number of PBKDF2 hardening rounds to use, larger values increase
            // computation time, you should select a value that causes
            // computation to take >100ms
            val iterations = 1000

            // Generate a 256-bit key
            val keyLength = 256

            val spec = PBEKeySpec(password, salt, iterations, keyLength)
            return SecurePreferences.encode(SecretKeyFactory.getInstance(SECRETE_KEY)
                    .generateSecret(spec).encoded)
        }

        @Throws(NoSuchAlgorithmException::class)
        private fun generateAesKeyValue(): String {
            // Do *not* seed secureRandom! Automatically seeded from system entropy
            val random = SecureRandom()

            // Use the largest AES key length which is supported by the OS
            val generator = KeyGenerator.getInstance(ENCRYPTION_TYPE)
            try {
                generator.init(256, random)
            } catch (e: Exception) {
                try {
                    generator.init(192, random)
                } catch (e1: Exception) {
                    generator.init(128, random)
                }

            }

            return SecurePreferences.encode(generator.generateKey().encoded)
        }

        private fun encrypt(cleartext: String?): String? {
            if (cleartext == null || cleartext.length == 0) {
                return cleartext
            }
            try {
                val cipher = Cipher.getInstance(ENCRYPTION_TYPE)
                cipher.init(Cipher.ENCRYPT_MODE, SecretKeySpec(SecurePreferences.sKey, ENCRYPTION_TYPE))
                return SecurePreferences.encode(cipher.doFinal(cleartext.toByteArray(charset("UTF-8"))))
            } catch (e: Exception) {
                Log.w(SecurePreferences::class.java.getName(), ENCRYPT_MODE, e)
                return null
            }

        }

        private fun decrypt(ciphertext: String?): String? {
            if (ciphertext == null || ciphertext.length == 0) {
                return ciphertext
            }
            try {
                val cipher = Cipher.getInstance(ENCRYPTION_TYPE)
                cipher.init(Cipher.DECRYPT_MODE, SecretKeySpec(SecurePreferences.sKey, ENCRYPTION_TYPE))
                return String(cipher.doFinal(SecurePreferences.decode(ciphertext)), Charsets.UTF_8)
            } catch (e: Exception) {
                Log.w(SecurePreferences::class.java.getName(), DECRYPT_MODE, e)
                return null
            }
        }
    }
}
