package com.mapmarker.common.data.model.response

data class ListResponseModel(val cod: Int?,
                             val calctime: Double?,
                             val cnt: Double?,
                             val list: List<Result>) {

    data class Result(val id: Double,
                      val dt: Double?,
                      val name: String?,
                      val coord: Coord,
                      val main: Main,
                      val wind: Wind,
                      val clouds: Clouds,
                      val weather: List<Weather>)

    data class Coord(val Lon: Double?,
                     val Lat: Double?)

    data class Main(val temp: Double?,
                    val temp_min: Double?,
                    val temp_max: Double?,
                    val pressure: Double?,
                    val sea_level: Double?,
                    val grnd_level: Double?,
                    val humidity: Double?)

    data class Wind(val speed: Double?,
                    val deg: Double?)

    data class Rain(val h: Double?)

    data class Clouds(val today: Double?)

    data class Weather(val id: Double?,
                       val main: String?,
                       val description: String?,
                       val icon: String?)

}