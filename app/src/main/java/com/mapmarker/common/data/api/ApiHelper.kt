package com.mapmarker.common.data.api

import com.mapmarker.common.data.model.response.ListResponseModel
import io.reactivex.Single
import retrofit2.http.GET


interface ApiHelper {

    /*
       * Api key with place
       * */
    //@GET("https://api.openweathermap.org/data/2.5/weather?q=dhaka,bd&units=metric&appid=8118ed6ee68db2debfaaa5a44c832918")

    /*
    * Example Api key
    * */

    //@GET("https://api.openweathermap.org/data/2.5/box/city?bbox=12,32,15,37,10&appid=8118ed6ee68db2debfaaa5a44c832918")
    //
    /*
    * My own API key
    * */
    @GET("https://api.openweathermap.org/data/2.5/box/city?bbox=12,32,15,37,10&appid=0863701986d1b59801968f41e2925239")
    fun getWeatherList(): Single<ListResponseModel>
}
