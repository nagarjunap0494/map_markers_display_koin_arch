package com.mapmarker.common.data.preferance

import android.content.Context

class AppPreference constructor(mContext: Context) : PreferencesHelper {

    /*Secure Shared Pref Class
   * which will be used to store
   * encrypted and decrypted data*/
    val mSharedPreferences: SecurePreferences = SecurePreferences(mContext)


}
