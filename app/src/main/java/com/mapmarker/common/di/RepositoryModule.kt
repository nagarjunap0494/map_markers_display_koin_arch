package com.mapmarker.common.di

import com.mapmarker.common.data.preferance.AppRepository
import org.koin.dsl.module.module

val repositoryModule = module {
    single { AppRepository(get(),get(),get()) }
}