package com.mapmarker.common.di

import com.mapmarker.common.data.preferance.AppPreference
import com.mapmarker.common.helper.SchedulerHelper
import com.mapmarker.common.helper.SchedulerProvider
import com.mapmarker.feature.activity.MapsViewModel
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val rxModule = module {
    single { SchedulerProvider() as SchedulerHelper }
}

val prefModule = module {
    single { AppPreference(get()) }
}

val viewModelModule = module {
    viewModel { MapsViewModel(get(), get()) }
}