package com.mapmarker.common.di

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonDeserializer
import com.google.gson.JsonPrimitive
import com.google.gson.JsonSerializer
import com.mapmarker.BuildConfig
import com.mapmarker.common.data.api.ApiHelper
import com.mapmarker.common.data.preferance.AppPreference
import com.mapmarker.common.network.CustomOkHttpClient
import com.mapmarker.common.network.RequestInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit

val networkModule = module{

    //provide RequestInterceptor
    single { provideRequestInterceptor(get()) }

    //provide LoggingInterceptop
    single { provideLoggingInterceptor() }

    // provided OkHttp
    single { provideOkHttpClient(get(), get()) }

    // provided Gson
    single { provideGson() }

    // provided Retrofit
    single { provideRemoteDataSource(get(), get()) }
}

fun provideGson(): Gson {
    val builder = GsonBuilder()

    builder.registerTypeAdapter(Date::class.java, JsonDeserializer<Date> { json, _, _ ->
        json?.asJsonPrimitive?.asLong?.let {
            return@JsonDeserializer Date(it)
        }
    })

    builder.registerTypeAdapter(Date::class.java, JsonSerializer<Date> { date, _, _ ->
        JsonPrimitive(date.time)
    })

    return builder.create()
}

fun provideRequestInterceptor(preference: AppPreference): RequestInterceptor {
    return RequestInterceptor(preference)
}

fun provideLoggingInterceptor(): HttpLoggingInterceptor {
    val logInterceptor = HttpLoggingInterceptor()
    if (BuildConfig.DEBUG) {
        logInterceptor.level = HttpLoggingInterceptor.Level.BODY
    } else {
        logInterceptor.level = HttpLoggingInterceptor.Level.NONE
    }
    return logInterceptor
}

fun provideOkHttpClient(requestInterceptor: RequestInterceptor,
                    logInterceptor: HttpLoggingInterceptor): OkHttpClient {

    val builder = CustomOkHttpClient.getUnsafeOkHttpClient()

    //Adiciona os interceptors
    builder.addInterceptor(logInterceptor)
    builder.addInterceptor(requestInterceptor)

    builder.connectTimeout(2, TimeUnit.MINUTES)
    builder.readTimeout(1, TimeUnit.MINUTES)
    builder.readTimeout(1, TimeUnit.MINUTES)

    return builder.build()
}

fun provideRemoteDataSource(okHttpClient: OkHttpClient, gson: Gson): ApiHelper {
        return Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(BuildConfig.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(ApiHelper::class.java)
}



