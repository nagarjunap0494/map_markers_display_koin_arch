package com.mapmarker.common.network

import com.mapmarker.common.data.preferance.AppPreference
import com.mapmarker.feature.utils.constants.AppConstants
import okhttp3.Interceptor
import okhttp3.Response

class RequestInterceptor(val appPreference: AppPreference) : Interceptor {
    override fun intercept(chain: Interceptor.Chain?): Response {
        val request = chain?.request()
        val newRequest = request?.newBuilder()

        try {
            newRequest?.addHeader(AppConstants.HEADER_KEY_ACCEPT, AppConstants.HEADER_BODY_ACCEPT)
                ?.addHeader(AppConstants.HEADER_KEY_CONTENT_TYPE, AppConstants.HEADER_BODY_CONTENT_TYPE)
                //?.addHeader(AppConstants.HEADER_KEY_AUTHORIZATION, appPreference.getAccessToken()?:"")

        } catch (ex: Throwable) {
            ex.printStackTrace()
        }

        return chain?.proceed(newRequest!!.build())!!
    }
}