package com.mapmarker.common

import android.app.Application
import androidx.multidex.MultiDex
import com.mapmarker.common.di.*
import org.koin.android.ext.android.startKoin

class ArchitectureApp : Application(){

    override fun onCreate() {
        super.onCreate()

        MultiDex.install(this)

        startKoin(this,
            listOf(prefModule,
                rxModule,
                networkModule,
                repositoryModule,
                viewModelModule))
    }
}